/*
 * Project: school-tag-remote-station-arduino
 * Description: Arduino Mifare MFRC522 NFC reader/writer for Ntag
 * 213(ultralight) and Real time clock(RTC) module DS3231 is used for
 * timestamping for school-tag-remote-station-arduino. Author: Aaron Roller &
 * Vikas Singh website:https://schooltag.org/ Date: 2 Sep, 2019
 * =======================
 *
 *
 *   RFID-RC522 (SPI connection)
 *
 *   CARD RC522      Arduino (UNO)
 *     SDA  -----------  10 (Configurable, see SS_PIN constant)
 *     SCK  -----------  13
 *     MOSI -----------  11
 *     MISO -----------  12
 *     IRQ  -----------
 *     GND  -----------  GND
 *     RST  -----------  9 (onfigurable, see RST_PIN constant)
 *     3.3V ----------- 3.3v
 *
 *  ---------------------------------------
 *
 *  Real Time clock DS3231 (I2C connection)
 *  Provides live date, day and time to the arduino even if arduino
 *  is turned off using adafruit's RTClib library.
 *
 *   DS3231 module       Arduino(UNO)
 *      vcc  -----------  5v(or 3.3v)
 *      GND  -----------  GND
 *      SCL  -----------  SCL/A5(check pinout for other arduinos)
 *      SDA  -----------  SDA/A4(check pinout for other arduinos)
 *
 *
 *    note: This program is not exact implementation of schooltag remote
 * particle station it just reads/writes given data to Ntag213 for basic test.
 */
#include "RTClib.h"
#include <SPI.h>
#include <StationSound.h>
#include <TagIo.h>

#define SS_PIN 10
#define RST_PIN 6
#define BUZZER_PIN 8

RTC_DS3231 rtc; // rtc instance created
char daysOfTheWeek[7][12] = {"Sunday",   "Monday", "Tuesday", "Wednesday",
                             "Thursday", "Friday", "Saturday"};
MFRC522 mfrc522(SS_PIN, RST_PIN); // Create MFRC522 instance
TagIo io(mfrc522);
StationSound sound(BUZZER_PIN);

void setup() {
  Serial.begin(115200); // Initialize serial communications with the PC
  SPI.begin();          // Init SPI bus
  mfrc522.PCD_Init();   // Init MFRC522 card

  //--------------  RTC setup ----------------------
  if (!rtc.begin()) {
    Serial.println("Couldn't find RTC");
    while (1)
      ;
  }

  if (rtc.lostPower()) {
    Serial.println("RTC lost power, lets set the time!");
    setRtcTimeFromBuild();
  }

  Serial.println(F("School Tag Remote Station is ready to scan tags."));
  pinMode(LED_BUILTIN, OUTPUT);
}

void loop() {
  if (io.tagIsReady()) {
    if (!io.tagIsNew()) {
      sound.ofRepeatTag();
      return;
    }
  } else {
    return;
  }
  // indicate tag is being read
  digitalWrite(LED_BUILTIN, HIGH); // light up LED to indicate communication
  TagIo::Visit visit = TagIo::Visit();
  visit.timestamp = timestamp();
  visit.stationId = stationId();
  int indexOfWrite = io.writeVisit(visit);
  // TODO: beep number of records in tag
  if (indexOfWrite <= FAILED) {
    Serial.println(F("Write visit failed"));
    // FIXME: Write failure gets its own sound
    sound.ofRepeatTag();
  } else {
    sound.ofSuccessfulVisit(indexOfWrite + 1);
  }
  io.print();
  timeStamp();
  mfrc522.PICC_HaltA();

  digitalWrite(LED_BUILTIN, LOW); // disable LED to indicate writing is complete
}

/** The Unix timestamp.
 * https://www.unixtimestamp.com/index.php
 */
unsigned long timestamp() {
  DateTime now = rtc.now();
  return now.unixtime();
}

/**
 * The identifier of this station that the student is visiting.
 */
unsigned int stationId() { return 0x5456; }

/*This function exposes date time day data though their repective functions
 * and prints them on serial
 * uncomment the print statments to log timestamps on serial monitor
 */
void timeStamp() {
  DateTime now = rtc.now();
  // uncomment below function for timestamps
  Serial.print(now.year(), DEC);
  Serial.print('/');
  Serial.print(now.month(), DEC);
  Serial.print('/');
  Serial.print(now.day(), DEC);
  Serial.print(" (");
  Serial.print(daysOfTheWeek[now.dayOfTheWeek()]);
  Serial.print(") ");
  Serial.print(now.hour(), DEC);
  Serial.print(':');
  Serial.print(now.minute(), DEC);
  Serial.print(':');
  Serial.print(now.second(), DEC);
  Serial.println();

  /*
    We can also get a 'timestamp' out of the DateTime object
    by calling unixtime which counts the number of
    seconds (not counting leapseconds) since midnight,
    January 1st 1970
    */
  Serial.print(" since midnight 1/1/1970 = ");
  Serial.print(now.unixtime());
  Serial.print("s = ");
  Serial.print(now.unixtime() / 86400L);
  Serial.println("d");

  // calculate a date which is 7 days and 30 seconds into the future
  DateTime future(now + TimeSpan(7, 12, 30, 6));

  Serial.print(" now + 7d + 30s: ");
  Serial.print(future.year(), DEC);
  Serial.print('/');
  Serial.print(future.month(), DEC);
  Serial.print('/');
  Serial.print(future.day(), DEC);
  Serial.print(' ');
  Serial.print(future.hour(), DEC);
  Serial.print(':');
  Serial.print(future.minute(), DEC);
  Serial.print(':');
  Serial.print(future.second(), DEC);
  Serial.println();

  /*  The DS3231 RTC Ic has internal temperature sensor
 that can be used for temperature logging of the station */
  Serial.print("Temperature: ");
  Serial.print(rtc.getTemperature());
  Serial.println(" C");
}

/**
 * call this to set the time from the local time of the build.
 * This typically works since you build right now and the board starts up and
 * gets assigned the build.
 */
void setRtcTimeFromBuild() {
  DateTime localBuildTime = DateTime(F(__DATE__), F(__TIME__));
  long timeZoneOffsetSeconds = 7 * 3600; // US pacfic daylight time
  DateTime utcTime =
      DateTime(localBuildTime.unixtime() + timeZoneOffsetSeconds);
  rtc.adjust(utcTime);
}